#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char * getLine(FILE *in, char starting, int *isEOF){
	
	if(!in){
		perror("Can't open directory");
		exit(1);
	}
	
	int len = 2;
	
	//printf("Starting a new line\n\n");
	char *output = (char *) malloc(sizeof(char)*(len));
	
	
	//printf("New line started\n\n");
	output[0] = starting;
	output[1] = 0;
	
	//printf("Line primed\n\n");
	
	//printf("Getting the next character\n\n");
	char current = fgetc(in);
	do{
		if(feof(in)){
			//printf("End of file found\n\n");
			*isEOF = 1;
			break;
		}
		
		if(current == 0 || current == '\n'){
			//printf("End of line found\n\n");
			break;
		}
		else{
			
			len += 1;
			//printf("Line length increased to %d\n", len);
			output = (char *) realloc(output, sizeof(char) * (len));
			//printf("New line address is %u for %s\n", output, output);
			output[len - 2] = current;
			//printf("Adding %c\n", current);
			//printf("%c", current);
			//printf("%s\n", output);
		}
		
		current = fgetc(in);
	}while(1==1);
	
	return(output);
}

char ** loadDictionary(char *filename, int *size)
{
	printf("Loading the dictionary\n\n");
	
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	// Allocate memory for an array of strings (arr).
	// Read the dictionary line by line.
	// Expand array if necessary (realloc).
	// Allocate memory for the string (str).
	// Copy each line into the string (use strcpy).
	// Attach the string to the large array (assignment =).
	
	//declaring the array of pointers
	//allocating space for the string array
	char **stringArray = (char **) malloc(sizeof(char *));
	
	//stringArray = NULL;
	
	int lines = 0;
	int isEOF = 0;
	
	lines = 1;
	
	
	printf("Was able to allocate\n");
	
	/*//
	to get the lines as they are presented in the file I can read each character of the line
	while watching for either a null character, or a line break character
	
	the idea is that I'll be creating a 2D array of most efficient size to limit the amount of memory used by
	the program due to the high memory volume potential
	//*/
	
	//stepping for every character for each line
	do{
		//printf("Entering loop\n\n");
		//This is to ensure that we only do work if the file is not empty
		//If we're already past the starting position there is no need to redo this
		stringArray[lines - 1] = getLine(in, fgetc(in), &isEOF);
		//printf("[%s]\n", getLine(in, fgetc(in), &isEOF));
		
		//printf("Entry %d found\n", lines);
		//printf("Entry %d is [ %s ]\n", lines, stringArray[lines - 1]);
		
		
		if(!isEOF){
			
			lines++;
			//printf("Starting a new line\n");
			stringArray = (char **) realloc(stringArray, sizeof(char **) * lines);
			
		}
		else{
			break;
		}
		
	}while(1);
	
	// The size should be the number of entries in the array.
	*size = lines;
	
	for(int i = 0; i < lines; i++){
		
		//printf("Line %d :: [%s]\n", i, stringArray[i]);
		
	}
	
	printf("Success %d lines loaded to dictionary\n\n", lines);
	
	// Return pointer to the array of strings.
	return stringArray;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}